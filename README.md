TASK1: How to install TYPO3 on CentOS

TYPO3 is a free and open-source Web content management system written in PHP. TYPO3 allows you to set up various permission levels for backend users. Also, this content management system allows you to manage multiple websites within one installation and share users, extensions, and content between them.

                                         Step 1 – Install Apache, PHP, and MySQL
First, install the Epel repository:
           yum -y install epel-release
Install Remi repository too:
           yum install http://rpms.remirepo.net/enterprise/remi-release-7.rpm
After, enable PHP 7.2
           yum-config-manager --enable remi-php72
Apache, PHP, and MySQL are needed to install TYPO3. You can install all of them by running this command:
           yum -y install httpd mariadb mariadb-server php72  php php-zip php-intl php-gd php-json php-mysqli php-curl php-intl

-----------------------------------------------------------------

NOTE: If you have already installed any one this, then try to use this command for check
For PHP-> 
PHP Version check 
          php –-version 
NOTE: If you don’t find any output, then you need to install PHP in your machine
for finding and Installation php packages 
          yum whatprovides php
          yum install php

For Apache->
Apache Version check 
          httpd –v
NOTE: If you don’t find any output, then you need to install apache httpd in your machine
for finding and Installation httpd packages
          yum whatprovides httpd
          yum install httpd

For MariaDB->
mysql Version check 
          mysql –-version
NOTE: If you don’t find any output, then you need to install MariaDB in your machine
for finding and installation of MariaDB packages
          yum whatprovides mariadb
          yum install mariadb-server

----------------------------------------

After Completing required installation, Now open php.ini file:
PHP Config File
         vim /etc/php.ini
             memory_limit set to a minimum of 256MB
             max_execution_time set to a minimum 240 seconds
             max_input_vars set to a minimum 1500
             post_max_size set to 10M to allow uploads of 10MB
             upload_max_filesize set to 10M to allow uploads of 10MB

After start MariaDB and Apache:
         systemctl start mariadb httpd
And enable them:
         systemctl enable mariadb httpd
for checking status		
         systemctl status httpd
         systemctl status mariadb

Step 2 – Create a database
for changing and config mysql
         sudo mysql_secure_installation
You will need to create a database for TYPO3. Run this command to log in MySQL:
         mysql -u root -p
After, run these commands to create a database:
         CREATE DATABASE typo3 CHARACTER SET utf8 COLLATE utf8_general_ci;
         CREATE USER 'typo3user'@'localhost' IDENTIFIED BY 'password';
         GRANT ALL PRIVILEGES ON typo3.* TO 'typo3user'@'localhost';
         FLUSH PRIVILEGES;
         exit


Step 3 – Install TYPO3
Before installing, download the TYPO3 file. First, switch to a web directory:
        cd /var/www/html
Download the TYPO3 file:
        wget --content-disposition get.typo3.org/9
NOTE: If you find any error regarding wget, it’s because you need to install that in your virtual machine
        yum whatprovides wget
        yum install wget 

Extract the TYPO3 file:
        tar -zxvf typo3_*.tar.gz
NOTE: If you find any error regarding tar, it’s because you need to install that in your virtual machine
        yum whatprovides tar
        yum install tar
Rename extracted directory to typo3:
        mv typo3*/ typo3/
After run FIRST_INSTALL to install TYPO3:
       touch /var/www/html/typo3/FIRST_INSTALL
And set apache permissions to TYPO3 file:
       chown -R apache:apache /var/www/html/typo3



Step 4 – Finish install
Now, open any browser and go to http://youripaddress:port/typo3/. And click on No problems detected, continue with the installation button.
 
 
 Complete this step by using the created database.
 
 Select an database
 
 Now create an administrative user
 
 Open the TYPO3 Backend
 
 Now log in to TYPO3


Step 5 - Conclusion
You have successfully installed TYPO3 on CentOS.
Enjoy!!











